﻿using System.Collections;
using UnityEngine;

public class Piece : MonoBehaviour {

	#region ATTRIBUTES
	//Active variable to update and move
	public bool _isActive;

	//It is asigned by the GameManager
	public Board _board;
	public int _color { get; set; }

	//Falling velocity
	public float _velocity;
	#endregion

	#region ONENABLE / START
	//OnEnable
	void OnEnable()
	{
		_isActive = true;
	}

	// Use this for initialization
	void Start() {
		_velocity = 0.5f;
		//_board.RedrawGrid(transform);

		//Coroutines
		StartCoroutine(UpdatePiece());
		StartCoroutine(GoDown());
	}
	#endregion

	#region COLOR
	/// <summary>
	/// Sets color in all boxes contained in the piece
	/// </summary>
	/// <param name="color"></param>
	public void AssignColor(int color)
	{
		//Debug.Log("Color :"+ color);
		foreach (Box box in transform.GetComponentsInChildren<Box>())
		{
			_color = color;
			box.SetColor(color);
		}
	}
	#endregion

	#region REASSIGN BOARD
	/// <summary>
	/// Reassigns board and redraws current grid
	/// </summary>
	public bool ReassignBoard()
	{
		_board = GameManager._instance._currentBoard;
		if (CheckIsAValidPosition())
		{
			_board.RedrawGrid(transform);
			return true;
		}

		return false;
	}
	#endregion

	#region PIECE UPDATE
	/// <summary>
	/// Continues Coroutines
	/// </summary>
	public void ContinuePieceCoroutines()
	{
		StartCoroutine(UpdatePiece());
		StartCoroutine(GoDown());
	}

	/// <summary>
	/// Update coroutine
	/// </summary>
	/// <returns></returns>
	IEnumerator UpdatePiece()
	{
		while (true)
		{
			//CONTROLS
			//Left
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				if (!MoveLeft())
				{
					ChangeIfIsOnTheLimit();
				}
			}

			//Right
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				if (!MoveRight())
				{
					ChangeIfIsOnTheLimit();
				}
			}

			//Uo
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				Rotate90();
			}

			//Down
			if (Input.GetKey(KeyCode.DownArrow))
			{
				_velocity = 0.03f;
			}
			else if (Input.GetKeyUp(KeyCode.DownArrow))
			{
				_velocity = 0.5f;
			}

			yield return null;
		}
	}

	/// <summary>
	/// Moves down the piece
	/// </summary>
	private IEnumerator GoDown()
	{
		while (true)
		{
			transform.position += Vector3.down;

			if (!CheckIsAValidPosition())
			{
				transform.position += Vector3.up;
				_isActive = false;

				if (!isOnGame())
				{
					StopAllCoroutines();
				}
			
				//Check Lines
				_board.SearchForLines();

				//if color belongs to the respective board
				if (_color == _board._numBoard)
				{
					//Gives its childrens to Board object
					UnparentPiece();
					
					if (!GameManager._instance._deletingPenalties) {
						//Instantiate a new piece
						GameManager._instance.InstantiatePiece();
					}
				}
				else
				{
					Debug.Log("Penalty");

					Delete2Boxes();
					yield return new WaitForSeconds(0.2f);

					UpdatePenaltyBoxes();
					yield return new WaitForSeconds(0.5f);

					//starts to set penalties in GameManager instance
					GameManager._instance.SetPenalties();
				}

				//Destroy boxes "container"
				Destroy(gameObject);
			}
			else
			{
				_board.RedrawGrid(transform);
			}

			yield return new WaitForSeconds(_velocity);
		}

	}
	#endregion

	#region PIECE CONTROL
	/// <summary>
	/// Moves the piece to the left if the Left Key on keyboard is pressed
	/// </summary>
	private bool MoveLeft()
	{
		transform.position += Vector3.left;

		if (!CheckIsAValidPosition())
		{
			transform.position += Vector3.right;
			return false;
		}

		_board.RedrawGrid(transform);
		return true;
	}

	/// <summary>
	/// Moves the piece to the right if the Right Key on keyboard is pressed
	/// </summary>
	private bool MoveRight()
	{
			transform.position += Vector3.right;

			if (!CheckIsAValidPosition())
			{
				transform.position += Vector3.left;
				return false;
			}
			
			_board.RedrawGrid(transform);
			return true;
	}

	/// <summary>
	/// Rotates the piece 90 grades if the Space Key on keyboard is pressed
	/// </summary>
	private bool Rotate90()
	{
			transform.Rotate(0, 0, -90);

			if (!CheckIsAValidPosition())
			{
				transform.Rotate(0, 0, 90);
				return false;
			}

			_board.RedrawGrid(transform);
			return true;
			
	}
	#endregion

	#region VALIDATE POSITION / ROTATION

	/// <summary>
	/// Checks if is a valid position. Returns false if one of the box in transform is occupying
	/// a space in grid previously occupied (and not by itself), or if one of the box is out of the limits x and y.
	/// </summary>
	/// <returns></returns>
	private bool CheckIsAValidPosition()
	{
		Transform boxT;
		IntVector2 posVect;

		for (int i = 0; i < transform.childCount; i++)
		{
			boxT = transform.GetChild(i);

			posVect = new IntVector2(boxT.position.x, boxT.position.y);

			// if is out of limits x or limits y
			if (//_board._numBoard == 0 || _board._numBoard == GameManager._instance._boardsList.Count
				posVect.x <= -1 || posVect.x >= _board._width
				|| posVect.y <= -1)
			{
				return false;
			}

			if (posVect.y < _board._height //if is inside of the height
				&& _board._grid[posVect.x, posVect.y] != null //if it is occupied
					&& _board._grid[posVect.x, posVect.y].parent != transform) //and it is not itself
			{
				return false;
			}

		} //end for

		return true;
	}

	/// <summary>
	/// Check if is a valid position to continue the game
	/// </summary>
	public bool isOnGame()
	{
		if (!_isActive) { 
			Transform boxT;
			IntVector2 posVect;

			for (int i = 0; i < transform.childCount; i++)
			{

				boxT = transform.GetChild(i);

				posVect = new IntVector2(boxT.position.x, boxT.position.y);

				if (posVect.y >= _board._height)
				{
					StopAllCoroutines();
					GameManager._instance.GameOver();
					return false;
				}

			}

			return true;
		}

		return true;
	}
	#endregion

	#region MOVE TO ANOTHER BOARD
	/// <summary>
	/// if is on a limit allowed the change will be done
	/// </summary>
	public void ChangeIfIsOnTheLimit()
	{
		if (CheckIsOntheLimit() == -1 && _board._numBoard > 0)
		{
			MoveToLimitRight();
		}
		else if (CheckIsOntheLimit() == 1 && _board._numBoard < GameManager._instance._boardsList.Count-1)
		{
			MoveToLimitLeft();
		}
		else
			return;
	}

	/// <summary>
	/// Checks if the piece is on the limit. Returns -1 if is on the left limit, 
	/// returns 1 if is on the right limit. Returns 0 if it is not on the limit.
	/// </summary>
	/// <returns></returns>
	public int CheckIsOntheLimit()
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			if (transform.GetChild(i).position.x == 0)
			{
				//Debug.Log("The box " + transform.GetChild(i) .name + " is on the limit.");
				return -1;
			}
			else if (transform.GetChild(i).position.x == _board._width-1)
			{
				//Debug.Log("The box " + transform.GetChild(i).name + " is on the limit.");
				return 1;
			}
		}

		return 0;
	}

	/// <summary>
	/// MOve the piece to the left
	/// </summary>
	public void MoveToLimitLeft()
	{
		//"Pause" coroutines
		GameManager._instance.StopAllCoroutines();
		StopAllCoroutines();

		while (MoveLeft());

		GameManager._instance.ChangeBoard(_board._numBoard, _board._numBoard +1);

		//"continue" coroutines
		GameManager._instance.ContinueGame();
		ContinuePieceCoroutines();

	}

	/// <summary>
	/// Move the piece to the right
	/// </summary>
	public void MoveToLimitRight()
	{
		//"Pause" coroutines
		GameManager._instance.StopAllCoroutines();
		StopAllCoroutines();

		while (MoveRight());

		GameManager._instance.ChangeBoard(_board._numBoard, _board._numBoard-1);

		//"continue" coroutines
		GameManager._instance.ContinueGame();
		ContinuePieceCoroutines();
	}
	#endregion

	#region UNPARENT
	/// <summary>
	/// Give and assign as children each one of the boxes parenting
	/// with the transform.
	/// </summary>
	public void UnparentPiece()
	{
		int lenght = transform.childCount;
		foreach(Transform t in transform.GetComponentsInChildren<Transform>())
		{
			t.parent = _board.transform;
		}
	}
	#endregion

	#region DELETE BOXES (IN PENALTIES)
	/// <summary>
	/// Delete the last 2 children of the piece.
	/// </summary>
	public void Delete2Boxes()
	{
		Transform t1 = transform.GetChild(2);
		Transform t2 = transform.GetChild(3);

		//for clean grid
		IntVector2 v1 = new IntVector2(t1.position.x, t1.position.y);
		IntVector2 v2 = new IntVector2(t2.position.x, t2.position.y);

		//clean grid of the boxes
		_board._grid[v1.x, v1.y] = null;
		_board._grid[v2.x, v2.y] = null;

		//destroy objects
		Destroy(t1.gameObject);
		Destroy(t2.gameObject);
	}

	#endregion

	#region UPDATE PENALTY BOXES

	/// <summary>
	/// Adjust the penalty boxes
	/// </summary>
	public void UpdatePenaltyBoxes()
	{
		Box b1, b2;

		//box 2
		b2 = transform.GetChild(0).GetComponent<Box>();

		b2._isANewPenalty = false;
		b2._board = _board;
		b2.StartUpdate();

		//box 1
		b1 = transform.GetChild(0).GetComponent<Box>();

		b1._isANewPenalty = false;
		b1._board = _board;
		b1.StartUpdate();

	}
	#endregion

}

