﻿using System.Collections;
using UnityEngine;

public class Box : MonoBehaviour {

	#region ATTRIBUTES
	//Activate
	public bool _isActive;

	//Is a Penalty?
	public bool _isANewPenalty { get; set; }

	//Color/Board
	public int _color;

	//Board
	public Board _board { get; set; }

	#endregion

	#region AWAKE / START
	// Use this for initialization
	void Start () {
		_isActive = false;
	}
	#endregion

	#region COLOR
	/// <summary>
	/// Sets color of the box
	/// </summary>
	/// <param name="color"></param>
	public void SetColor(int numColor)
	{
		_color = numColor;
		Color color = GameManager._instance._colorsList[numColor];
		GetComponent<SpriteRenderer>().color = color;
	}
	#endregion

	#region UPDATE

	/// <summary>
	/// Start the corutine UpdateMethod()
	/// </summary>
	public void StartUpdate()
	{
		StartCoroutine(UpdateMethod());
	}

	/// <summary>
	/// Update Coroutine
	/// </summary>
	/// <returns></returns>
	IEnumerator UpdateMethod()
	{
		while (true) {
				transform.position += Vector3.down;

				if (!CheckIsAValidPosition())
				{
					transform.position += Vector3.up;

					_isActive = false;

					transform.parent = _board.transform;
					_board._penaltiesList.Add(this);

					if (_isANewPenalty) { 
						GameManager._instance.GiveAPenalty();
					}

					StopAllCoroutines();
				}
				else
				{
					_board.RedrawGrid(this);
				}
			yield return null;
			
		}
	}
	#endregion

	#region VALIDATE POSITION
	/// <summary>
	/// Check if its position is valid, eles returns to 
	/// </summary>
	public bool CheckIsAValidPosition()
	{
		IntVector2 posVect;

		posVect = new IntVector2(transform.position.x, transform.position.y);

		// if is out of limits x or limits y
		if (posVect.x <= -1 || posVect.x >= _board._width
			|| posVect.y <= -1)
		{
			return false;
		}

		if (posVect.y < _board._height //if is inside of the height
			&& _board._grid[posVect.x, posVect.y] != null) //if it is occupied
		{
			return false;
		}

		return true;
	}
	#endregion
	
}
