﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#region IntVector2 structure
public struct IntVector2
{
	public int x;
	public int y;

	public IntVector2(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public IntVector2(float X, float Y)
	{
		x = Mathf.RoundToInt(X);
		y = Mathf.RoundToInt(Y);
	}
}
#endregion

public class GameManager : MonoBehaviour {

	#region ATTRIBUTES
	//Singleton
	public static GameManager _instance;

	//UI
	[Header("UI")]
	public int _score;
	public Text _scoreText;

	[Header("UI Arrows")]
	public Image _dontLeft;
	public Image _dontRight;

	//Colors
	[Header("Colors")]
	public List<Color32> _colorsList;

	//Next Piece
	[Header("Next Piece")]
	public List<Image> _piecesImageList;
	public int _nextPiece;
	public int _nextColorPiece;

	//Instantiate pieces
	[Header("Pieces")]
	public List<Piece> _piecesList;
	public Transform _instantiator;

	//Current Pieces and Boards
	[Header("Current Elements")]
	public Piece _currentPiece;

	//Boards
	[Header("Boards")]
	public Board _currentBoard;
	public List<Board> _boardsList;

	//Penalties variables
	[Header("Penalties variables")]
	public Box _boxPrefab;
	public Box _currentBox;
	
	public int XcurrentBoardNum;
	public int Xfrom;
	public int Xto;

	[Header("Penalties sustraction")]
	public int _previousBoard;
	public Transform _cursor;
	public bool _deletingPenalties;

	//Main Menu / Game Over
	[Header("Main Menu")]
	public GameObject mainMenuPanel;

	[Header("GameOver")]
	public GameObject gameOverPanel;

	//public List<Box> _penaltiesList;

	//Click
	[Header("Touch")]
	public Camera cam;
	RaycastHit2D hit;

	public Camera cam2;
	RaycastHit2D hit2;

	[Header("CPU Player")]
	public CPUManager _cpuManager;
	#endregion

	#region AWAKE/START
	private void Awake()
	{
		_instance = this;
	}

	// Use this for initialization
	void Start() {
		StartCoroutine(MainTitle());
	}
	#endregion

	#region START GAME

	IEnumerator MainTitle()
	{
		while (mainMenuPanel.activeInHierarchy)
		{
			if (Input.GetKeyDown(KeyCode.Return))
			{
				mainMenuPanel.SetActive(false);
				StartGame();
				_cpuManager.StartGame();
			}
			yield return null;
		}
	}
	/// <summary>
	/// Starts the game
	/// </summary>
	public void StartGame()
	{
		StopCoroutine(MainTitle());

		//score
		_score = 0;
		_nextPiece = -1; //undefined

		_deletingPenalties = false;

		//assign current board
		for (int i=0; i < _boardsList.Count; i++) {
			_boardsList[i]._numBoard = i;
		}
		_currentBoard = _boardsList[1];

		gameOverPanel.SetActive(false);

		//test
		InstantiatePiece();

		StartCoroutine(UpdateMethod());
	}
	#endregion

	#region SCORE
	/// <summary>
	/// Makes points
	/// </summary>
	public void MakePoints()
	{
		_score += 100;
		_scoreText.text = _score.ToString();
	}
	#endregion

	#region INSTANTIATOR
	/// <summary>
	/// Show the next piece to instantiate
	/// </summary>
	public void ShowNextPiece()
	{
		_nextPiece = Random.Range(0, _piecesList.Count - 1);

		foreach (Image pieceSprite in _piecesImageList)
		{
			pieceSprite.gameObject.SetActive(false);
		}

		_piecesImageList[_nextPiece].gameObject.SetActive(true);

		//Colors in UI
		_nextColorPiece = Random.Range(0, _colorsList.Count);
		_piecesImageList[_nextPiece].color = _colorsList[_nextColorPiece];

	}

	/// <summary>
	/// Instantiates a piece
	/// </summary>
	public void InstantiatePiece()
	{
		Piece newPiece;

		//if next Piece is not assigned yet
		if (_nextPiece < 0)
		{
			int pieceSelected = Random.Range(0, _piecesList.Count - 1);

			newPiece = Instantiate(_piecesList[pieceSelected],
										_instantiator.position,
										Quaternion.identity)
										as Piece;

			//Assign Color to pieces
			int numColor = Random.Range(0, _colorsList.Count);
			newPiece.AssignColor(numColor);
		}
		//else, if the next piece is assigned already
		else
		{
			newPiece = Instantiate(_piecesList[_nextPiece],
										_instantiator.position,
										Quaternion.identity)
										as Piece;

			//Assign previous color selected to pieces
			newPiece.AssignColor(_nextColorPiece);
		}

		//just assigns current board (to new piece object)
		// and new piece (to Game Manager) respectively
		if (newPiece != null)
		{
			newPiece._board = _currentBoard;
			_currentPiece = newPiece;
		}

		//new next piece
		ShowNextPiece();
	}

	/// <summary>
	/// Instantiate a box as a penaltie
	/// </summary>
	/// <param name="color"></param>
	public Box InstantiateBox(int color)
	{
		//Sets a new position
		Vector3 newPosition = new Vector3(Random.Range(0, _currentBoard._width - 1),
											_instantiator.position.y,
											_instantiator.position.z);

		//Instantiate
		Box newBox = Instantiate(_boxPrefab, newPosition, Quaternion.identity);

		//SetColor and board
		newBox.SetColor(color);
		newBox._board = _currentBoard;

		//Actives the box
		newBox._isANewPenalty = true;
		newBox._isActive = true;

		newBox.StartUpdate();

		return newBox;
	}

	#endregion

	#region UPDATE
	/// <summary>
	/// Continues the game
	/// </summary>
	public void ContinueGame()
	{
		StartCoroutine(UpdateMethod());
	}

	/// <summary>
	/// Update method
	/// </summary>
	/// <returns></returns>
	private IEnumerator UpdateMethod()
	{
		while (true)
		{

			if (_deletingPenalties)
			{
				ControlBoards();
			}

			if (Input.GetMouseButtonDown(0))
			{
				Vector3 pos  = cam.ScreenToWorldPoint(Input.mousePosition);
				hit = Physics2D.Raycast(pos, Vector2.zero);

				Vector3 pos2 = cam.ScreenToWorldPoint(Input.mousePosition);
				hit2 = Physics2D.Raycast(pos, Vector2.zero);

				if (hit != null && hit.collider != null && 
					(hit.collider.transform.parent == _currentBoard.transform ||
					hit.collider.transform.parent == _cpuManager._currentBoard.transform ) )
				{ 
					//Debug.Log("Hit Something : " + hit.collider.name);
					IntVector2 posVec = new IntVector2(hit.collider.transform.position.x, hit.collider.transform.position.y);

					
					for (int i=0; i < _currentBoard._penaltiesList.Count; i++)
					{
						if(hit.transform == _currentBoard._penaltiesList[i].transform )
						{
							_currentBoard._penaltiesList.RemoveAt(i);
						}
					}

					_currentBoard._grid[posVec.x, posVec.y] = null;
					Destroy(hit.transform.gameObject);
				}

			}

			yield return null;
		}
	}

	/// <summary>
	/// Controls to move and change boards
	/// </summary>
	public void ControlBoards()
	{	
		//Left
		if (Input.GetKeyDown(KeyCode.A))
		{
			if (_currentBoard != _boardsList[0])
			{
				//to left
				ChangeBoard(-1);
			}
		}

		//Right
		if (Input.GetKeyDown(KeyCode.D))
		{
			if (_currentBoard != _boardsList[_boardsList.Count-1])
			{
				//to right
				ChangeBoard(1);
			}
		}

		
	}

	#endregion
	
	#region CHANGE BOARD
	/// <summary>
	/// Changes current board. If the given parameter is equal to -1, 
	/// the change is for the left, and if it's equal to 1, 
	/// the change is for the right
	/// </summary>
	/// <param name="side"></param>
	public void ChangeBoard(int side)
	{
		_currentBoard.StopAllCoroutines();

		//to the LEFT
		if (side == -1)
		{
			//change the cam view
			for (int i = 0; i < _boardsList.Count; i++)
			{
				_boardsList[i].transform.position += (Vector3.right * 11);
			}
			
			//change of board
			_currentBoard = _boardsList[_currentBoard._numBoard - 1];

			//if is not deleting penatlies
			if (!_deletingPenalties)
			{
				//clean previous grid
				_boardsList[_currentBoard._numBoard + 1].CleanBoardOfActivePieces();

				//REASSIGN AND VALIDATION
				//Reassign previous board to the current piece
				//if is not a valid position for the piece
				if (!_currentPiece.ReassignBoard())
				{
					//undo changes
					ChangeBoard(1);
					StartCoroutine(showDont(_dontLeft));
				}
			} else
			{
				if (_currentBoard.HasPenalties())
				{
					_currentBoard.SearchForPenalties();
				}
				else
				{
					_cursor.gameObject.SetActive(false);
				}
			}

		}
		//to the RIGHT
		else if (side == 1)
		{
			//change the cam view
			for (int i = 0; i < _boardsList.Count; i++)
			{
				_boardsList[i].transform.position += (Vector3.left * 11);
			}

			//change of board
			_currentBoard = _boardsList[_currentBoard._numBoard + 1];

			//if is not deleting penatlies
			if (!_deletingPenalties)
			{
				//clean previous grid
				_boardsList[_currentBoard._numBoard - 1].CleanBoardOfActivePieces();

				//REASSIGN AND VALIDATION
				//Reassign previous board to the current piece
				//if is not a valid position for the piece
				if (!_currentPiece.ReassignBoard())
				{
					//undo changes
					ChangeBoard(-1);
					StartCoroutine(showDont(_dontRight));
				}
			} else
			{
				if (_currentBoard.HasPenalties())
				{
					_currentBoard.SearchForPenalties();
				} else
				{
					_cursor.gameObject.SetActive(false);
				}
			}
		}
		else
		{
			Debug.Log("ChangeBoard : Side value " + side  +" is not valid.");
		}
	}

	/// <summary>
	/// Change Board from given board to another given board.
	/// </summary>
	/// <param name="fromBoardNum"></param>
	/// <param name="toBoardNum"></param>
	public int ChangeBoard(int fromBoardNum, int toBoardNum)
	{
		int diff = -(fromBoardNum - toBoardNum);

		//change the cam view
		for (int i = 0; i < _boardsList.Count; i++)
		{
			//Debug.Log(Vector3.right*(11 * diff));
			_boardsList[i].transform.position += (Vector3.left * (11 * diff));
		}

		//change of board
		_currentBoard = _boardsList[toBoardNum];

		//clean previous grid
		_boardsList[fromBoardNum].CleanBoardOfActivePieces();

		if (_deletingPenalties)
		{
			//_deletingPenalties = false;
			_currentBoard.SearchForLines();
		}

		_currentPiece._board = _currentBoard;
		return _currentBoard._numBoard;

	}

	/// <summary>
	/// For 0.6 seconds active the UI Image Dont given
	/// </summary>
	/// <param name="dont"></param>
	/// <returns></returns>
	IEnumerator showDont(Image dont)
	{
		dont.gameObject.SetActive(true);
		yield return new WaitForSeconds(.6f);

		dont.gameObject.SetActive(false);
	}

	#endregion

	#region PUT PENALTIES
	/// <summary>
	/// Pauses the game and initialises global variables to instantiate 
	/// the respective penalties
	/// </summary>
	public void SetPenalties()
	{
		//pause game
		StopAllCoroutines();
		_currentPiece.StopAllCoroutines();

		XcurrentBoardNum = _currentBoard._numBoard; //save current board

		Xfrom = XcurrentBoardNum;
		Xto = 0;

		GiveAPenalty();
	}

	/// <summary>
	/// Changes the currrent board and instantiates a new penalty.
	/// </summary>
	public void GiveAPenalty()
	{	
		if (Xto < _boardsList.Count)
		{
			//if is not the current Board
			if (Xto != XcurrentBoardNum)
			{
				Xfrom = ChangeBoard(Xfrom, Xto);
				Xto++;

				_currentBox = InstantiateBox(XcurrentBoardNum);
			}
			else
			{
				Xto++;
				GiveAPenalty();
			}
		}
		else
		{
			//Continue game
			StartCoroutine(UpdateMethod());
			InstantiatePiece();

		}

	}

	#endregion

	#region DELETE PENALTIES
	/// <summary>
	/// Check if there are penalties.
	/// </summary>
	public bool AreTherePenalties()
	{
		for (int i = 0; i < _boardsList.Count; i++)
		{
			if(_boardsList[i].HasPenalties())
			{
				//_cursor.gameObject.SetActive(true);
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Move cursor to the current penalty position
	/// </summary>
	public void MoveCursor()
	{
		_cursor.transform.position = _currentBoard._penaltiesList[_currentBoard._currentPenaltySelected].transform.position;
	}

	/// <summary>
	/// Resume the instantiation of pieces
	/// </summary>
	public void AfterDeleteLinesAndPenalties(){
		_deletingPenalties = false;
		StopAllCoroutines();

		StartCoroutine(UpdateMethod());
		InstantiatePiece();
	}
	#endregion

	#region GAME OVER
	/// <summary>
	/// Set active true the Game Over Panel.
	/// Starts a Coroutine to restart the game.
	/// </summary>
	public void GameOver()
	{
		StopAllCoroutines();

		gameOverPanel.SetActive(true);

		StartCoroutine(PlayAgain());
	}

	/// <summary>
	/// Restart the game if Enter key is pressed
	/// </summary>
	/// <returns></returns>
	IEnumerator PlayAgain()
	{
		while (true)
		{
			if (Input.GetKeyDown(KeyCode.Return))
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}

			yield return null;
		}
	}
	#endregion


}