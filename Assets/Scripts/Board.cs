﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour {

	#region ATTRIBUTES
	//Number of board
	[HideInInspector]
	public int _numBoard{ get; set;}

	//Size of board
	[Header("Board Dimension")]
	public int _width = 10;
	public int _height = 20;

	//Grid
	public Transform[,] _grid;

	//Penalties
	[Header("Penalties")]
	public int _currentPenaltySelected;
	public List<Box> _penaltiesList;
	#endregion

	#region AWAKE / START
	// Use this for initialization
	void Start ()
	{
		_grid = new Transform[_width, _height];
		_currentPenaltySelected = 0;
	}
	#endregion

	#region DRAW GRID
	/// <summary>
	/// Redraws grid given a Piece
	/// </summary>
	/// <param name="board"></param>
	public void RedrawGrid(Transform pieceTransform)
	{
		//Clean grid.
		//Check x row.
		for (int i = 0; i < _width; i++)
		{
			// Check y row.
			for (int j = 0; j < _height; j++)
			{
				//Check if there is an element in matrix.
				if (_grid[i, j] != null)
				{
					//If is an element of the pieceTransform:
					if (_grid[i, j].parent == pieceTransform)
					{
						//Cleans this element.
						_grid[i, j] = null;
					}
				}
			}
		}

		//redefine
		for (int k = 0; k < pieceTransform.childCount; k++)
		{
			Transform boxT = pieceTransform.GetChild(k);
			IntVector2 posVect = new IntVector2(boxT.position.x,
													boxT.position.y);

			if (posVect.y < _height)
			{
				_grid[posVect.x, posVect.y] = boxT;
			}
		}

	}

	/// <summary>
	/// Redraws grid given a box
	/// </summary>
	/// <param name="board"></param>
	public void RedrawGrid(Box box)
	{
		Transform boxTransform = box.transform;

		//Clean grid.
		//Check x row.
		for (int i = 0; i < _width; i++)
		{
			// Check y row.
			for (int j = 0; j < _height; j++)
			{
				//Check if there is an element in matrix.
				if (_grid[i, j] != null && _grid[i, j] == boxTransform)
				{
						//Cleans this element.
						_grid[i, j] = null;
				}
			}
		}

		IntVector2 posVect = new IntVector2(boxTransform.position.x,
												boxTransform.position.y);

		if (posVect.y < _height)
		{
			_grid[posVect.x, posVect.y] = boxTransform;
		}

	}
	#endregion

	#region SEARCH FOR LINES AND DELETE THEM
	/// <summary>
	/// Search for lines 
	/// to delete
	/// </summary>
	public bool SearchForLines()
	{
		int counter;

		for ( int i = 0; i < _height ; i++ )
		{
			counter = 0;

			for ( int j = 0; j < _width ; j++ )
			{
				if (_grid[j,i] != null 
					&& _grid[j,i].GetComponent<Box>()._color == _numBoard) //check the colour (if there is not a penalty)
				{
					counter++;
				}
			}

			if(counter == _width)
			{
				DeleteLine(i);
				return true;
			}

		}
		
		return false;
			
	}

	/// <summary>
	/// Delete a entire row. Calls to MakePoints and Updgrade
	/// </summary>
	/// <param name="row"></param>
	public void DeleteLine(int row)
	{
		for (int i= 0; i < _width; i++)
		{
			Destroy(_grid[i,row].gameObject);
			_grid[i, row] = null;
		}

		GameManager._instance.MakePoints();
		UpdateGridAfterLines(row);
	}

	/// <summary>
	/// Redraws Grid after make Lines
	/// </summary>
	/// <param name="rowsDeletedList"></param>
	public void UpdateGridAfterLines(int row)
	{
		//Grid
		for (int j = row; j < _height; j++)
		{
			for (int i = 0; i < _width; i++)
			{
				if (_grid[i, j] != null && (j - 1) >= 0)
				{
					_grid[i, j].transform.position += Vector3.down;
					_grid[i, j - 1] = _grid[i, j];
					_grid[i, j] = null;
				}
			}
		}

		///Check if there are penalties
		if (GameManager._instance.AreTherePenalties() && !GameManager._instance._deletingPenalties)
		{
			GameManager._instance._previousBoard = _numBoard;
			GameManager._instance._deletingPenalties = true;
			SearchForPenalties();
			return;
		}	

		/*
		if(GameManager._instance._deletingPenalties)
		{
			GameManager._instance._deletingPenalties = false;
			GameManager._instance.InstantiatePiece();
		}*/

		//if(!SearchForLines()) GameManager._instance.InstantiatePiece();
	}

	#endregion

	#region CHANGE BOARD
	/// <summary>
	/// When active pieces change of board, reset grid of that elements that
	/// not belong to transform(board) children.
	/// </summary>
	public void CleanBoardOfActivePieces()
	{
		for (int i = 0; i < _width; i++)
		{
			for(int j = 0; j < _height; j++)
			{
				if (_grid[i,j] != null && _grid[i,j].parent != transform)
				{
					_grid[i, j] = null;
				}
			}
		}
	}
	#endregion

	#region SEARCH FOR PENALTIES
	/// <summary>
	/// Check if the board has penalties
	/// </summary>
	/// <returns></returns>
	public bool HasPenalties()
	{
		if(_penaltiesList.Count > 0)
		{
			return true;
		}
		return false;
	}

	/// <summary>
	/// Searches for penalties
	/// </summary>
	public void SearchForPenalties()
	{
		_currentPenaltySelected = 0;

		if (_penaltiesList.Count > 0)
		{
			GameManager._instance._cursor.gameObject.SetActive(true);
			GameManager._instance.MoveCursor();

			StartCoroutine(SelectPenalties());
		}
	}

	/// <summary>
	/// Select penalties from board penalties list
	/// </summary>
	/// <returns></returns>
	IEnumerator SelectPenalties()
	{
		while (true)
		{
			//left
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				if (_currentPenaltySelected > 0)
				{
					_currentPenaltySelected--;
				}
				else { _currentPenaltySelected = _penaltiesList.Count - 1; }

				GameManager._instance.MoveCursor();
			}

			//right
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				if (_currentPenaltySelected < _penaltiesList.Count-1)
				{
					_currentPenaltySelected++;
				}
				else { _currentPenaltySelected = 0; }

				GameManager._instance.MoveCursor();

			}

			
			//delete
			if (Input.GetKeyDown(KeyCode.Return))
			{
				DeletePenalty();
			}
			yield return null;
		}
	}

	/// <summary>
	/// Clean grid and destroy selected penalty
	/// </summary>
	public void DeletePenalty()
	{
		//clean and delete
		GameObject penaltyObj = _penaltiesList[_currentPenaltySelected].gameObject;

		IntVector2 posVec = new IntVector2(penaltyObj.transform.position.x, penaltyObj.transform.position.y);

		_grid[posVec.x, posVec.y] = null;

		_penaltiesList.RemoveAt(_currentPenaltySelected);
		Destroy(penaltyObj);

		//Adjust boxes above the penalty
		AdjustBoxesAbove(posVec);
		SearchForLines();

		//deactive cursor
		GameManager._instance._cursor.gameObject.SetActive(false);

		StopAllCoroutines();

		//back to the previous board for Search Lines
		if(_numBoard != GameManager._instance._previousBoard)
		{
			GameManager._instance.ChangeBoard(_numBoard, GameManager._instance._previousBoard);
		}

		//GameManager._instance._deletingPenalties = false;

		if (!SearchForLines())
		{
			GameManager._instance.AfterDeleteLinesAndPenalties();
		}
			
	}

	/// <summary>
	/// Adjusts boxes above given the position of the current penalty
	/// </summary>
	public void AdjustBoxesAbove(IntVector2 vectPen)
	{
		Transform t;
		IntVector2 vect2;

		for (int i = 0; i < transform.childCount; i++)
		{
			t = transform.GetChild(i);
			vect2 = new IntVector2(t.position.x, t.position.y);

			if (vect2.x == vectPen.x && vect2.y > vectPen.y)
			{
				t.position += Vector3.down;
				_grid[vect2.x, vect2.y] = null;
				_grid[vectPen.x, vectPen.y] = t;
			}
		}
	}
	#endregion

}
