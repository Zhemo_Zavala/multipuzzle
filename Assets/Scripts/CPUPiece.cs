﻿using System.Collections;
using UnityEngine;

public class CPUPiece : MonoBehaviour {

	#region ATTRIBUTES
	//Active variable to update and move
	public bool _isActive;

	//It is asigned by the GameManager
	public Board _board;
	public int _color { get; set; }

	//Falling velocity
	public float _velocity;
	#endregion

	#region ONENABLE / START
	//OnEnable
	void OnEnable()
	{
		_isActive = true;
	}

	// Use this for initialization
	void Start()
	{
		_velocity = 0.8f;
		//_board.RedrawGrid(transform);

		//Coroutines
		//StartCoroutine(UpdatePiece());
		StartCoroutine(GoDown());
	}
	#endregion

	#region COLOR
	/// <summary>
	/// Sets color in all boxes contained in the piece
	/// </summary>
	/// <param name="color"></param>
	public void AssignColor(int color)
	{
		//Debug.Log("Color :"+ color);
		foreach (Box box in transform.GetComponentsInChildren<Box>())
		{
			_color = color;
			box.SetColor(color);
		}
	}
	#endregion

	#region REASSIGN BOARD
	/// <summary>
	/// Reassigns board and redraws current grid
	/// </summary>
	public bool ReassignBoard()
	{
		_board = CPUManager._instance._currentBoard;
		if (CheckIsAValidPosition())
		{
			_board.RedrawGrid(transform);
			return true;
		}

		return false;
	}
	#endregion

	#region PIECE UPDATE
	/// <summary>
	/// Continues Coroutines
	/// </summary>
	public void ContinuePieceCoroutines()
	{
		//StartCoroutine(UpdatePiece());
		StartCoroutine(GoDown());
	}

	/*
	/// <summary>
	/// Update coroutine
	/// </summary>
	/// <returns></returns>
	IEnumerator UpdatePiece()
	{
		while (true)
		{
			//CONTROLS
			//Left
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				if (!MoveLeft())
				{
					//ChangeIfIsOnTheLimit();
				}
			}

			//Right
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				if (!MoveRight())
				{
					//ChangeIfIsOnTheLimit();
				}
			}

			//Uo
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				Rotate90();
			}

			//Down
			if (Input.GetKey(KeyCode.DownArrow))
			{
				_velocity = 0.03f;
			}
			else if (Input.GetKeyUp(KeyCode.DownArrow))
			{
				_velocity = 0.5f;
			}

			yield return null;
		}
	}
	*/

	/// <summary>
	/// Moves down the piece
	/// </summary>
	private IEnumerator GoDown()
	{
		while (true)
		{
			transform.position += Vector3.down;

			if (!CheckIsAValidPosition())
			{
				transform.position += Vector3.up;
				_isActive = false;

				if (!isOnGame())
				{
					StopAllCoroutines();
				}

				//Check Lines
				_board.SearchForLines();

				/*
				//if color belongs to the respective board
				if (_color == _board._numBoard)
				{*/
					//Gives its childrens to Board object
					UnparentPiece();

					//if (!GameManager._instance._deletingPenalties)
					//{
						//Instantiate a new piece
						CPUManager._instance.InstantiatePiece();
					//}
				/*}
				else
				{
					Debug.Log("Penalty");

					Delete2Boxes();
					yield return new WaitForSeconds(0.2f);

					UpdatePenaltyBoxes();
					yield return new WaitForSeconds(0.5f);

					//starts to set penalties in GameManager instance
					GameManager._instance.SetPenalties();
				}*/

				//Destroy boxes "container"
				Destroy(gameObject);
			}
			else
			{
				_board.RedrawGrid(transform);
			}

			yield return new WaitForSeconds(_velocity);
		}

	}
	#endregion

	#region PIECE CONTROL
	/// <summary>
	/// Moves the piece to the left if the Left Key on keyboard is pressed
	/// </summary>
	private bool MoveLeft()
	{
		transform.position += Vector3.left;

		if (!CheckIsAValidPosition())
		{
			transform.position += Vector3.right;
			return false;
		}

		_board.RedrawGrid(transform);
		return true;
	}

	/// <summary>
	/// Moves the piece to the right if the Right Key on keyboard is pressed
	/// </summary>
	private bool MoveRight()
	{
		transform.position += Vector3.right;

		if (!CheckIsAValidPosition())
		{
			transform.position += Vector3.left;
			return false;
		}

		_board.RedrawGrid(transform);
		return true;
	}

	/// <summary>
	/// Rotates the piece 90 grades if the Space Key on keyboard is pressed
	/// </summary>
	private bool Rotate90()
	{
		transform.Rotate(0, 0, -90);

		if (!CheckIsAValidPosition())
		{
			transform.Rotate(0, 0, 90);
			return false;
		}

		_board.RedrawGrid(transform);
		return true;

	}
	#endregion

	#region VALIDATE POSITION / ROTATION

	/// <summary>
	/// Checks if is a valid position. Returns false if one of the box in transform is occupying
	/// a space in grid previously occupied (and not by itself), or if one of the box is out of the limits x and y.
	/// </summary>
	/// <returns></returns>
	private bool CheckIsAValidPosition()
	{
		Transform boxT;
		IntVector2 posVect;

		for (int i = 0; i < transform.childCount; i++)
		{
			boxT = transform.GetChild(i);

			posVect = new IntVector2(boxT.position.x, boxT.position.y);

			// if is out of limits x or limits y
			if (//_board._numBoard == 0 || _board._numBoard == GameManager._instance._boardsList.Count
				posVect.x <= -1 || posVect.x >= _board._width
				|| posVect.y <= -1)
			{
				return false;
			}

			if (posVect.y < _board._height //if is inside of the height
				&& _board._grid[posVect.x, posVect.y] != null //if it is occupied
					&& _board._grid[posVect.x, posVect.y].parent != transform) //and it is not itself
			{
				return false;
			}

		} //end for

		return true;
	}

	/// <summary>
	/// Check if is a valid position to continue the game
	/// </summary>
	public bool isOnGame()
	{
		if (!_isActive)
		{
			Transform boxT;
			IntVector2 posVect;

			for (int i = 0; i < transform.childCount; i++)
			{

				boxT = transform.GetChild(i);

				posVect = new IntVector2(boxT.position.x, boxT.position.y);

				if (posVect.y >= _board._height)
				{
					StopAllCoroutines();
					GameManager._instance.StopAllCoroutines();
					GameManager._instance.GameOver();
					return false;
				}

			}

			return true;
		}

		return true;
	}
	#endregion

	#region UNPARENT
	/// <summary>
	/// Give and assign as children each one of the boxes parenting
	/// with the transform.
	/// </summary>
	public void UnparentPiece()
	{
		int lenght = transform.childCount;
		foreach (Transform t in transform.GetComponentsInChildren<Transform>())
		{
			t.parent = _board.transform;
		}
	}
	#endregion

}
