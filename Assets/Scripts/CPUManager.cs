﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CPUManager : MonoBehaviour {

	#region ATTRIBUTES
	//Singleton
	public static CPUManager _instance;

	//UI
	[Header("UI")]
	public int _score;
	public Text _scoreText;

	//Colors
	[Header("Colors")]
	public List<Color32> _colorsList;

	//Next Piece
	[Header("Next Piece")]
	public List<Image> _piecesImageList;
	public int _nextPiece;
	public int _nextColorPiece;

	//Instantiate pieces
	[Header("Pieces")]
	public List<CPUPiece> _piecesList;
	public Transform _instantiator;

	//Current Pieces and Boards
	[Header("Current Elements")]
	public CPUPiece _currentPiece;

	//Boards
	[Header("Boards")]
	public Board _currentBoard;
	//public List<Board> _boardsList;

	//Click
	[Header("Touch")]
	public Camera cam;
	RaycastHit2D hit;

	#endregion

	#region AWAKE/START
	private void Awake()
	{
		_instance = this;
	}

	// Use this for initialization
	void Start()
	{
		//StartCoroutine(MainTitle());
	}
	#endregion

	#region START GAME
	/// <summary>
	/// Starts the game
	/// </summary>
	public void StartGame()
	{
		//StopCoroutine(MainTitle());

		//score
		_score = 0;
		_nextPiece = -1; //undefined

		//test
		InstantiatePiece();
	}
	#endregion

	#region SCORE
	/// <summary>
	/// Makes points
	/// </summary>
	public void MakePoints()
	{
		_score += 100;
		_scoreText.text = _score.ToString();
	}
	#endregion

	#region INSTANTIATOR
	/// <summary>
	/// Show the next piece to instantiate
	/// </summary>
	public void ShowNextPiece()
	{
		_nextPiece = Random.Range(0, _piecesList.Count - 1);

		foreach (Image pieceSprite in _piecesImageList)
		{
			pieceSprite.gameObject.SetActive(false);
		}

		_piecesImageList[_nextPiece].gameObject.SetActive(true);

		//Colors in UI
		_nextColorPiece = Random.Range(0, _colorsList.Count);
		_piecesImageList[_nextPiece].color = _colorsList[_nextColorPiece];

	}

	/// <summary>
	/// Instantiates a piece
	/// </summary>
	public void InstantiatePiece()
	{
		CPUPiece newPiece;
		Vector3 newPosition = new Vector3(Mathf.RoundToInt(Random.Range(0, _currentBoard._width)),
											_instantiator.position.y,
											_instantiator.position.z);

		//if next Piece is not assigned yet
		if (_nextPiece < 0)
		{
			int pieceSelected = Random.Range(0, _piecesList.Count - 1);

			newPiece = Instantiate(_piecesList[pieceSelected],
										newPosition,
										Quaternion.identity)
										as CPUPiece;

			//Assign Color to pieces
			int numColor = 0;
			newPiece.AssignColor(numColor);
		}
		//else, if the next piece is assigned already
		else
		{
			newPiece = Instantiate(_piecesList[_nextPiece],
										newPosition,
										Quaternion.identity)
										as CPUPiece;

			//Assign previous color selected to pieces
			newPiece.AssignColor(_nextColorPiece);
		}

		//just assigns current board (to new piece object)
		// and new piece (to Game Manager) respectively
		if (newPiece != null)
		{
			newPiece._board = _currentBoard;
			_currentPiece = newPiece;
		}

		//new next piece
		ShowNextPiece();
	}

	#endregion

	#region UPDATE
	/// <summary>
	/// Continues the game
	/// </summary>
	public void ContinueGame()
	{
		//TODO Continue Coroutines
	}

	#endregion

	#region SEARCH AND MOVE/ROTATE THE PIECE
	/// <summary>
	/// Evaluate and changes the best score and applies the movements
	/// </summary>
	public void ControlPiece()
	{
		//TODO Evaluate the best score
		
		//TODO Make the change Rotation
		
		//TODO Make the change position

		//TODO Accelerate
	}

	/// <summary>
	/// Evaluate the grid depending the depth of search
	/// </summary>
	public void EvaluateGrid()
	{

	}

	#endregion

	#region GAME OVER
	/// <summary>
	/// Set active true the Game Over Panel.
	/// Starts a Coroutine to restart the game.
	/// </summary>
	public void GameOver()
	{
		StopAllCoroutines();
		GameManager._instance.GameOver();
	}

	/// <summary>
	/// Restart the game if Enter key is pressed
	/// </summary>
	/// <returns></returns>
	IEnumerator PlayAgain()
	{
		while (true)
		{
			if (Input.GetKeyDown(KeyCode.Return))
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}

			yield return null;
		}
	}
	#endregion
}
